#include "HyperXKeyboardController.h"
#include "RGBController.h"
#include "RGBController_HyperXKeyboard.h"
#include <vector>
#include <hidapi/hidapi.h>

// Vendor ID of Hyper X
#define HYPERX_KEYBOARD_VID         0x0951

// HyperX Keyboard IDs
#define HYPERX_ALLOY_ELITE_PID      0x16BE
#define HYPERX_ALLOY_FPS_PID        0x16dc


typedef struct
{
    unsigned short  usb_vid;
    unsigned short  usb_pid;
    unsigned char   usb_interface;
    const char *    name;
} hyperx_node_device;

// Logic for looking for different IDs
#define HYPERX_KB_DEVICES (sizeof(device_list) / sizeof(device_list[ 0 ]))

static const hyperx_node_device device_list[] =
{
    /*-------------------------------------------------------------------------------------------------------*\
    | Keyboards                                                                                               |
    \*-------------------------------------------------------------------------------------------------------*/
    { HYPERX_KEYBOARD_VID,		HYPERX_ALLOY_ELITE_PID,				 1,      "HyperX Alloy Elite"            },
    { HYPERX_KEYBOARD_VID,		HYPERX_ALLOY_FPS_PID,			 	 1,      "HyperX Alloy FPS RGB"          }

};

void DetectHyperXKeyboardControllers(std::vector<RGBController*>& rgb_controllers)
{
    hid_device_info* info;
    hid_device* dev;

    hid_init();

    for(std::size_t device_idx = 0; device_idx < HYPERX_KB_DEVICES; device_idx++)
    {
        dev = NULL;

        info = hid_enumerate(device_list[device_idx].usb_vid, device_list[device_idx].usb_pid);

        //Look For HyperX Device
        while(info)
        {
            if((info->vendor_id == device_list[device_idx].usb_vid)
            &&(info->product_id == device_list[device_idx].usb_pid)
            &&(info->interface_number == device_list[device_idx].usb_interface))
            {
                dev = hid_open_path(info->path);
                break;
            }
            else
            {
                info = info->next;
            }
        }

        if( dev )
        {
            HyperXKeyboardController* controller = new HyperXKeyboardController(dev);

            if(controller->GetDeviceType() != DEVICE_TYPE_UNKNOWN)
            {
                RGBController_HyperXKeyboard* rgb_controller = new RGBController_HyperXKeyboard(controller);

                rgb_controller->name = device_list[device_idx].name;

                rgb_controllers.push_back(rgb_controller);
            }
        }
    }
}

